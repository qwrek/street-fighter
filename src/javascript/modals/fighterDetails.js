import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source:image } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: image } });

  nameElement.innerText = "My name is (what?): " + name + '\n';
  fighterDetails.append(nameElement);

  attackElement.innerText = "My damage is (hun?): " +  attack + '\n';
  fighterDetails.append(attackElement);

  defenseElement.innerText = "Defense: " +  defense + '\n';
  fighterDetails.append(defenseElement);

  healthElement.innerText = "Health: " +  health;
  fighterDetails.append(healthElement);

  //imageElement.innerText = image;
  fighterDetails.append(imageElement);

  return fighterDetails;
}
