import { createElement } from '../helpers/domHelper';
import {showModal} from "./modal";

export  function showWinnerModal(fighter) {
    const {name} = fighter;

    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

    nameElement.innerText = name;

    showModal({title: 'Winner is: ', bodyElement:nameElement});
}