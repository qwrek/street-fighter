export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;

    let move = 0;

    while(true) {
        if(move % 2) {
            secondFighterHealth -= getDamage(firstFighter, secondFighter);
        }
        else {
            firstFighterHealth -= getDamage(secondFighter, firstFighter);
        }
        if(firstFighterHealth <= 0) {
            return secondFighter;
        }
        else if(secondFighterHealth <= 0) {
            return firstFighter;
        }
        move++;
    }
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
    const criticalHitChance = Math.random() + 1;
    const power = fighter.attack * criticalHitChance;
    return power;
}

export function getBlockPower(fighter) {
    const dodgeChance = Math.random() + 1;
    const power = fighter.defense * dodgeChance;
    return power;
}
